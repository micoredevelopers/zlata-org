function setMarquee(selector) {
    $(selector).marquee({
        //duration in milliseconds of the marquee
        duration: 10000,
        //gap in pixels between the tickers
        gap: 0,
        //time in milliseconds before the marquee will start animating
        delayBeforeStart: 0,
        //'left' or 'right'
        direction: 'left',
        //true or false - should the marquee be duplicated to show an effect of continues flow
        duplicated: true,
        startVisible: true
    });
}

function setSlider(selector) {
    $(selector).slick({
        infinite: true,
        dots: false,
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: '.reviews-slider__pag .arrow-prev',
        nextArrow: '.reviews-slider__pag .arrow-next'
    })
}

function setSliderEvents(selector) {
    const slider = $(selector)
    slider.on('init', function (e, { slideCount, currentSlide }) {
        $('.slider-pagination .count').text(slideCount)
        $('.slider-pagination .current').text(currentSlide + 1)
    })


    slider.on('afterChange', function (e, { currentSlide }) {
        $('.slider-pagination .current').text(currentSlide + 1)
    })
}

function lastPosts(name) {
    $.ajax({
        url: "https://www.instagram.com/"+ name +"/?__a=1",
        success(data) {
            const edges = data.graphql.user.edge_owner_to_timeline_media.edges
            edges.forEach((item, i) => {
                let itemSrc = item.node.display_url
                if(i<6) {
                    $('#instagram-photos').append(
                        `<a target="_blank"  href="https://www.instagram.com/p/` +
                        item.node.shortcode +
                        '/" class="instafeed__box" target="_blank" id="' +
                        i +
                        '"><img class="instafeed__img" src="' +
                        itemSrc +
                        '" /></a>'
                    )
                }

            })
        }
    })
}

const convertImages = (query, callback) => {
    const images = document.querySelectorAll(query)

    images.forEach(image => {
        fetch(image.src)
            .then(res => res.text())
            .then(data => {
                const parser = new DOMParser()
                const svg = parser.parseFromString(data, 'image/svg+xml').querySelector('svg')

                if (svg) {
                    if (image.id) svg.id = image.id
                    if (image.className) svg.classList = image.classList
                    image.parentNode.replaceChild(svg, image)
                }
            })
            .then(callback)
            .catch(error => console.error(error))
    })
}

function toggleModal(btn) {
    $(btn).on('click', function () {
        const modal = $($(this).attr('data-modal'));
        const html = $('html');
        modal.addClass('active');
        html.addClass('active');
        modal.on('click', function (e) {
            const target = $(e.target);
            if(target.hasClass('close-modal') || target.hasClass('active')) {
                modal.removeClass('active')
                html.removeClass('active');
            }
        })
    })
}

function toggleMenu(burger) {
    const toggler = $(burger);
    const menu = $('.header-nav')

    toggler.on('click', function () {
        menu.toggleClass('active')
        $(this).toggleClass('active')
    })
}

function sendCallback(selector) {
    const form = $(selector);
    const buttonImg = form.find('.form-btn__img');
    const comment = form.find('.comment-input input');
    const btn = buttonImg.parents('button');

    form.on('submit', function (e) {
        e.preventDefault();

        buttonImg.attr('src', 'src/images/icons/loader.svg');
        buttonImg.addClass('loading');
        btn.attr('disabled', 'true');

        $.ajax({
            method: 'POST',
            data: form.serialize(),
            url: form.attr('action'),
            success: function (res) {
                buttonImg.attr('src', 'src/images/icons/done.svg');
                buttonImg.removeClass('loading')
                comment.val('');
                btn.attr('disabled', 'false');
            },
            error: function (err) {

            }
        })
    })
}

function setPhoneMask(selector) {
    $(selector).mask('+38(999) 999-9999')
}

$(document).ready(function () {
    setMarquee('.section-slider')
    window.innerWidth <=768 ? setSliderEvents('.reviews-slider__sm') :     setSliderEvents('.reviews-slider')
    window.innerWidth <=786 ? setSlider('.reviews-slider__sm') : setSlider('.reviews-slider') ;
    lastPosts('zlata.org.space')
    convertImages('.svg');
    toggleModal('button[data-modal]')
    toggleMenu('.burger-menu')
    sendCallback('#callback-modal')
    sendCallback('.contacts-form')
    setPhoneMask('.phone')
})

